from datetime import datetime

_d=datetime.now()

f1=open("time_gap.txt",'r')

lns=f1.readlines()[0][:-1]

f1.close()

prd=datetime.fromisoformat(lns)

_k=_d-prd
if _k.days>0:
	if _k.days>=30 and _k.days/30<12:
		print(f"system was updated {_k.days/30} months ago...")
	elif _k.days/30>12:
		print(f"system was updated {(_k.days/30)/12} years ago...")
	elif _k.days>=7 and _k.days<30:
		print(f"system was updated {int(_k.days/7)} weeks {int(_k.days%7)} days {int((_k.seconds/60)/60)} hours ago...")
	else:
		print(f"system was updated {_k.days} days {int((_k.seconds/60)/60)} hours ago...")
else:
	if _k.seconds>60 and _k.seconds/60<60:
		print(f"system was updated {int(_k.seconds/60)} minutes {int(_k.seconds%60)} seconds ago...")
	elif _k.seconds<60:
		print(f"system was updated {_k.seconds} seconds ago...")
	else:
		print(f"system was updated {int((_k.seconds/60)/60)} hours {int(_k.seconds/60)%60} minutes ago...")
