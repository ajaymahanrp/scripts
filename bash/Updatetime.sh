#!/bin/bash

Sys_upgrade(){
	sudo pacman -Syu $*
	if [ $? -eq 0 ]; then
		echo $(date +%F)T$(date +%T) > $HOME/Scripts/time_gap.txt
		echo Added time
	else
		echo "Couldn't update time"
	fi
}

UTime(){
	cd $HOME/Scripts/
	python Updatetime.py
	cd $OLDPWD
	
}