#!/bin/python

# impedence matching calc

import math
import microfeedline as msf


def quatwtrZ0(Zin,Zl):

    return math.sqrt(Zin*Zl)


def dquatwtr(Zl,er,frq,h):
    Zin=50
    Z1=quatwtrZ0(Zin,Zl)
    cvf=1000

    pd=90
    wd=msf.Wd_ratio(er,Z1)
    wq=msf.mswid(wd,h)
    lq=msf.mslen(pd,frq,msf.e_eff(er,wd))

    return f"Width(mm): {wq*cvf}\nLength(mm): {lq*cvf}\nZ0: {Z1}\nld/4(mm): {(300/(frq/1e9))/4}"

